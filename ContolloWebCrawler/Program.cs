﻿using System;
using System.Windows.Forms;

namespace ContolloWebCrawler
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormWebCrawler());
            //Application.Run(new XtraFormMDIMain());
        }
    }
}
