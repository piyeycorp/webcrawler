﻿using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;

namespace ContolloWebCrawler
{
    public class Spider
    {
        private string _url;
        private string _parsedUrl;
        private HtmlDocument _document;

        public Spider(string URL)
        {
            _url = URL;
        }

        public void SetDocument(params SpiderParam[] args)
        {
            _parsedUrl = _url;
            args
                 .ToList()
                 .ForEach(a =>
                 {
                     _parsedUrl = _parsedUrl.Replace($"{{{{{a.Key}}}}}", a.Value);
                 })
                 ;

            var web = new HtmlWeb();
            _document = web.Load(_parsedUrl);
        }

        public List<SpiderElement> GetData()
        {
            List<SpiderElement> indeedElements = new List<SpiderElement>();

            var nodes = _document.DocumentNode.SelectNodes("//div[contains(@class, 'jobsearch-SerpJobCard')]");

            nodes.ToList().ForEach(n =>
            {
                var id = n.Attributes["data-jk"].Value;
                var jobTitle = n.SelectSingleNode(".//h2[contains(@class,'jobtitle')]/a");
                var company = n.SelectSingleNode(".//div[@class='companyInfoWrapper']/div/span[@class='company']/a") ?? n.SelectSingleNode("//div[@class='companyInfoWrapper']/div/span[@class='company']");
                var location = n.SelectSingleNode(".//div[@class='companyInfoWrapper']/span[@class='location']");
                var summary = n.SelectSingleNode(".//div[contains(@class,'paddedSummary')]/span[contains(@class,'summary')]");
                var salary = n.SelectSingleNode(".//div[@class='salarySnippet']/span[contains(@class,'salary')]");
                indeedElements.Add(new SpiderElement
                {
                    ID = id,
                    JobTitle = jobTitle?.InnerText?.Trim(),
                    Company = company?.InnerText?.Trim(),
                    Location = location?.InnerText?.Trim(),
                    Summary = summary?.InnerText?.Trim(),
                    Salary = salary?.InnerText?.Trim(),
                    URL = "https://www.indeed.com/viewjob?jk={{jk}}&tk=1d2li7pbhag14800&from=vjnewtab".Replace("{{jk}}", id),
                });
            });

            return indeedElements;
        }
        
        public List<SpiderElement> GetData(params SpiderParam[] args)
        {
            List<SpiderElement> indeedElements = new List<SpiderElement>();
            SetDocument(args);

            var nodes = _document.DocumentNode.SelectNodes("//div[contains(@class, 'jobsearch-SerpJobCard')]");

            nodes.ToList().ForEach(n =>
            {
                var id = n.Attributes["data-jk"].Value;
                var jobTitle = n.SelectSingleNode(".//h2[contains(@class,'jobtitle')]/a");
                var company = n.SelectSingleNode(".//div[@class='companyInfoWrapper']/div/span[@class='company']/a") ?? n.SelectSingleNode("//div[@class='companyInfoWrapper']/div/span[@class='company']");
                var location = n.SelectSingleNode(".//div[@class='companyInfoWrapper']/span[@class='location']");
                var summary = n.SelectSingleNode(".//div[contains(@class,'paddedSummary')]/span[contains(@class,'summary')]");
                var salary = n.SelectSingleNode(".//div[@class='salarySnippet']/span[contains(@class,'salary')]");
                indeedElements.Add(new SpiderElement
                {
                    ID = id,
                    JobTitle = jobTitle?.InnerText?.Trim(),
                    Company = company?.InnerText?.Trim(),
                    Location = location?.InnerText?.Trim(),
                    Summary = summary?.InnerText?.Trim(),
                    Salary = salary?.InnerText?.Trim(),
                    URL = "https://www.indeed.com/viewjob?jk={{jk}}&tk=1d2li7pbhag14800&from=vjnewtab".Replace("{{jk}}", id),
                });
            });

            return indeedElements;
        }

        public List<Pagging> GetPagging()
        {
            List<Pagging> paggings = new List<Pagging>();
            var paginator = _document.DocumentNode.SelectNodes("//div[@class='pagination']/a | //div[@class='pagination']/b");

            paginator
                .Select((pag, i) => new { pag, i })
                .ToList()
                .ForEach(pag =>
                {
                    if (pag.pag.SelectSingleNode(".//span[@class='pn']/span[@class='np']") != null) return;
                    //Button btnPaging = new Button();
                    //btnPaging.Tag = "paging";
                    //btnPaging.Location = new System.Drawing.Point(12 + (pag.i * 28) + 5, 486);
                    ////btnPaging.Name = "button1";
                    //btnPaging.Size = new System.Drawing.Size(28, 23);
                    //btnPaging.TabIndex = 9;
                    //btnPaging.UseVisualStyleBackColor = true;

                    Pagging pagging = new Pagging { IsB = false, IsNP = false };

                    if (pag.pag.Name == "b")
                    {
                        //btnPaging.Text = pag.pag.InnerText.Trim();
                        //btnPaging.Enabled = false;

                        pagging.IsB = true;
                        pagging.Name = pag.pag.InnerText.Trim();
                    }
                    else
                    {
                        var pageNode = pag.pag.SelectSingleNode(".//span[@class='pn']");
                        if (pageNode != null)
                        {
                            //string page = pageNode.InnerText.Trim();
                            //btnPaging.Text = page;
                            //btnPaging.Enabled = true;

                            pagging.Name = pageNode.InnerText.Trim();
                            pagging.IsNP = pageNode.SelectSingleNode(".//span[@class='np']") != null;
                        }
                    }

                    //btnPaging.Click += (_, __) =>
                    //{
                    //    int.TryParse(((Button)_).Text, out var start);
                    //    GetDocument(what, where, quantity, (start - 1) * quantity);

                    //    Controls.OfType<Button>().ToList().ForEach(cmd => cmd.Enabled = true);
                    //    ((Button)_).Enabled = false;
                    //};

                    //paginationButtons.Add(btnPaging);
                    paggings.Add(pagging);
                });

            return paggings;
        }
    }
}
