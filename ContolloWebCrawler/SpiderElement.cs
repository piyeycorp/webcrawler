﻿using System.ComponentModel;

namespace ContolloWebCrawler
{
    public class SpiderElement
    {
        [Browsable(false)]
        public string ID { get; set; }
        public string JobTitle { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string Summary { get; set; }
        public string Salary { get; internal set; }
        public string URL { get; set; }
    }
}
