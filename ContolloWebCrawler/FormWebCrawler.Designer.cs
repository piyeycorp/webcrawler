﻿namespace ContolloWebCrawler
{
    partial class FormWebCrawler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormWebCrawler));
            this.label1 = new System.Windows.Forms.Label();
            this.cmdGo = new System.Windows.Forms.Button();
            this.imageList16x16 = new System.Windows.Forms.ImageList(this.components);
            this.imageList32x32 = new System.Windows.Forms.ImageList(this.components);
            this.txtWhat = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtWhere = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "What:";
            // 
            // cmdGo
            // 
            this.cmdGo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdGo.ImageKey = "Button-Next-icon (1).png";
            this.cmdGo.ImageList = this.imageList16x16;
            this.cmdGo.Location = new System.Drawing.Point(386, 16);
            this.cmdGo.Name = "cmdGo";
            this.cmdGo.Size = new System.Drawing.Size(56, 23);
            this.cmdGo.TabIndex = 2;
            this.cmdGo.Text = "&Go";
            this.cmdGo.UseVisualStyleBackColor = true;
            this.cmdGo.Click += new System.EventHandler(this.cmdGo_ClickAsync);
            // 
            // imageList16x16
            // 
            this.imageList16x16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16x16.ImageStream")));
            this.imageList16x16.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList16x16.Images.SetKeyName(0, "Button-Next-icon (1).png");
            this.imageList16x16.Images.SetKeyName(1, "Actions-edit-clear-icon.png");
            // 
            // imageList32x32
            // 
            this.imageList32x32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList32x32.ImageStream")));
            this.imageList32x32.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList32x32.Images.SetKeyName(0, "Close-2-icon.png");
            this.imageList32x32.Images.SetKeyName(1, "Close_Box_Red.ico");
            this.imageList32x32.Images.SetKeyName(2, "Excel-icon.png");
            this.imageList32x32.Images.SetKeyName(3, "Actions-application-exit-icon.png");
            // 
            // txtWhat
            // 
            this.txtWhat.Location = new System.Drawing.Point(50, 18);
            this.txtWhat.Name = "txtWhat";
            this.txtWhat.Size = new System.Drawing.Size(138, 20);
            this.txtWhat.TabIndex = 7;
            this.txtWhat.Text = "Developer";
            this.txtWhat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWhatOrWhere_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Where:";
            // 
            // txtWhere
            // 
            this.txtWhere.Location = new System.Drawing.Point(242, 18);
            this.txtWhere.Name = "txtWhere";
            this.txtWhere.Size = new System.Drawing.Size(138, 20);
            this.txtWhere.TabIndex = 7;
            this.txtWhere.Text = "Remote";
            this.txtWhere.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWhatOrWhere_KeyDown);
            // 
            // FormWebCrawler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 52);
            this.Controls.Add(this.txtWhere);
            this.Controls.Add(this.txtWhat);
            this.Controls.Add(this.cmdGo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormWebCrawler";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WebCrawler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdGo;
        private System.Windows.Forms.ImageList imageList32x32;
        private System.Windows.Forms.ImageList imageList16x16;
        private System.Windows.Forms.TextBox txtWhat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtWhere;
    }
}

