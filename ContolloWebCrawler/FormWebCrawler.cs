﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ContolloWebCrawler
{
    public partial class FormWebCrawler : Form
    {
        const string DETAIL_LINK = "https://www.indeed.com/viewjob?jk={jk}&from=vjs&vjs=1&tk=1d2l1q60k20es003";
        public FormWebCrawler()
        {
            InitializeComponent();
        }

        private void cmdGo_ClickAsync(object sender, EventArgs e)
        {
            int quantity = 50;
            Spider indeedSpider = new Spider("https://www.indeed.com/jobs?q={{what}}&l={{where}}&limit={{limit}}");
            indeedSpider.SetDocument(new SpiderParam[]
            {
                new SpiderParam { Key = "what", Value = txtWhat.Text.Trim() },
                new SpiderParam { Key = "where", Value = txtWhere.Text.Trim() },
                new SpiderParam { Key = "limit", Value = quantity.ToString() },
                new SpiderParam { Key = "start", Value = 0.ToString() },
            });

            var pagging = indeedSpider.GetPagging();

            List<SpiderElement> datos = new List<SpiderElement>();
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(sfd.FileName)) File.Delete(sfd.FileName);

                pagging
                .Select((p, i) => new { p, i })
                .ToList()
                .ForEach(p =>
                {
                    datos.AddRange(indeedSpider.GetData(new SpiderParam[]
                    {
                            new SpiderParam { Key = "what", Value = txtWhat.Text.Trim() },
                            new SpiderParam { Key = "where", Value = txtWhere.Text.Trim() },
                            new SpiderParam { Key = "limit", Value = quantity.ToString() },
                            new SpiderParam { Key = "start", Value = ((p.i-1)*50).ToString() },
                    })
                    );
                });

                try
                {
                using (var writer = new StreamWriter(sfd.FileName))
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(datos);
                        MessageBox.Show("Done!","Export csv");
                }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void txtWhatOrWhere_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cmdGo_ClickAsync(cmdGo, null);
            }
        }
    }
}
